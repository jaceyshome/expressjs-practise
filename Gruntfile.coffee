module.exports = (grunt)->
  #variables
  ROOT_FILE = "bin/index.html"

  #used by e2e to target a spec
  spec = grunt.option('spec');
  spec = "*" unless spec?

  #config
  grunt.initConfig
    pkd: grunt.file.readJSON "package.json"
    less:
      dev:
        files:
          "bin/assets/css/style.css": "src/main.less"
        options:
          sourceMap:true

      deploy:
        files:
          "bin/assets/css/style.css": "src/main.less"
        options:
          sourceMap:false
          cleancss:true
          compress:true

    coffee:
      dev:
        expand:true
        cwd:"src"
        src:["**/*.coffee"]
        dest:"bin/assets/js/app/"
        ext:".js"
        options:
          sourceMap:true
          bare:true
      deploy:
        expand:true
        cwd:"src"
        src:["**/*.coffee"]
        dest:"bin/assets/js/app/"
        ext:".js"
        options:
          sourceMap:false
          bare:true

    jade:
      dev:
        expand:true
        cwd:"src"
        src:["**/*.jade"]
        dest:"templates/"
        ext:".html"
        options:
          pretty:true
      deploy:
        expand:true
        cwd:"src"
        src:["**/*.jade"]
        dest:"templates/"
        ext:".html"
        options:
          pretty:false
          data:
            deploy:true

    rename:
      index:
        src:"templates/main.html"
        dest:ROOT_FILE

    ngtemplates:
      dev:
        cwd:"templates"
        src:"**/*.html"
        dest:"bin/assets/js/app/templates.js"
        options:
          module:"app"
          bootstrap:(module, script)->
            return "define(['angular'], function() {angular.module('templates', []).run([
                '$templateCache', 
                function($templateCache) {#{script}}
              ]);});"
          htmlmin:
            collapseBooleanAttributes:      true
            collapseWhitespace:             true
            removeAttributeQuotes:          true
            removeComments:                 true
            removeEmptyAttributes:          true
            removeRedundantAttributes:      true
            removeScriptTypeAttributes:     true
            removeStyleLinkTypeAttributes:  true
          url: (url)->
            url.replace '.html', ''


    ngmin:
      dev:
        expand: true,
        cwd: 'bin/assets/js/app/'
        src:["**/*.js"]
        dest: 'bin/assets/js/app/'

    requirejs:
      main:
        options:
          baseUrl: "bin/assets/js/app"
          mainConfigFile: "bin/assets/js/app/main.js"
          name: "main"
          wrap: true
          optimize : "uglify2"
          out: "bin/assets/js/deploy/main.js"
          include: ['../lib/almond/almond.js']
          insertRequire: ['main']
      ie8:
        options:
          baseUrl: "bin/assets/js/app/fix"
          mainConfigFile: "bin/assets/js/app/fix/ie8fix.js"
          name: "ie8fix"
          wrap: true
          optimize : "uglify2"
          out: "bin/assets/js/deploy/ie8fix.js"
          include: ['../../lib/almond/almond.js']
          insertRequire: ['ie8fix']

    yaml:
      dev:
        expand:true
        cwd:"src/data"
        src:"**/*.yml"
        dest:"bin/assets/data"

    watch:
      less:
        files:"**/*.less"
        tasks:["buildLess"]
        options:
          spawn:true
          interrupt:true
          #livereload: true
      coffee:
        files:"**/*.coffee"
        tasks:["watchCoffee"]
        options:
          spawn:true
          interrupt:true
          #livereload: true
      jade:
        files:"**/*.jade"
        tasks:["buildJade"]
        options:
          interrupt:true
          #livereload: true
      yaml:
        files:"**/*.yml"
        tasks:["yaml:dev"]
        options:
          interrupt:true
          #livereload: true
      texts:
        files:"src/**/*.md"
        tasks:["buildText"]
        options:
          interrupt:true
          #livereload: true
      bin:
        files:"bin/**/*"
        options:
          interrupt:true
          livereload: true

    concurrent:
      watch:
        tasks: ["watch:less", "watch:coffee", "watch:jade", "watch:yaml", "watch:texts", "watch:bin"]
        options:
          logConcurrentOutput: true
      test:
        tasks: ["watch:less", "watch:coffee", "watch:jade", "watch:yaml", "watch:texts", "karma:dev"]
        options:
          logConcurrentOutput: true

    scorm_manifest:
      dev:
        expand:true
        cwd:"tmp/bin"
        src: ['**/*.*']
        filter: 'isFile'
        options:
          path:"tmp/bin/"

    compress:
      scorm:
        expand:true
        cwd:"tmp/bin"
        src:"**"
        dest:"./"
        options:
          archive: 'scorm.zip'
          pretty: true
      package:
        expand:true
        cwd:"tmp/bin"
        src:"**"
        dest:"./"
        options:
          archive: 'package.zip'
          pretty: true

    clean:
      app:["bin/assets/js/app"]
      templates:["templates"]
      reports:["reports"]
      texts:["texts"]
      export:["tmp/bin/assets/js/lib", "tmp/bin/assets/js/app"]
      tmp:["tmp"]
      options:
        force:true

    copy:
      export:
        expand:true
        src:"bin/**/*"
        dest:"tmp/"
        filter:"isFile"
      exportExceptions: # recopy any files that are needed from js/lib that will be deleted
        files:[
          {
            src:"bin/assets/js/lib/video-js/video-js.swf"
            dest:"tmp/"
          },
          {
            src:"bin/assets/js/lib/scorm-api-wrapper/src/JavaScript/SCORM_API_wrapper.js"
            dest: "tmp/"
          }
        ]

    accessibility:
      templates:
        expand:true
        cwd:"templates"
        src:"**/*.html"
        dest:"reports/wcag/"
        ext:"-report.txt"
        options:
          accessibilityLevel: 'WCAG2AA'
          ignore : [
            'WCAG2AA.Principle2.Guideline2_4.2_4_2.H25.1.NoTitleEl'
            'WCAG2AA.Principle3.Guideline3_1.3_1_1.H57.2'
          ]
      index:
        expand:true
        src:ROOT_FILE
        dest:"reports/wcag/"
        ext:"-report.txt"
        options:
          accessibilityLevel: 'WCAG2AA'

    coffeelint:
      app:"src/**/*.coffee"
      options:
        force:true
        max_line_length:
          value:120

    htmlangular:
      templates:
        expand:true
        src:["templates/**/*.html"]
        options:
          tmplext:".html"
          reportpath: 'reports/html/html-angular-validate-templates-report.json'
      index:
        expand:true
        src:[ROOT_FILE]
        options:
          reportpath: 'reports/html/html-angular-validate-index-report.json'
      options:
        angular:false

    lesslint:
        src: ['src/main.less']
        options:
          imports: ['src/**/*.less']
          csslint:
            "unqualified-attributes":false
            "adjoining-classes":false
            "qualified-headings":false
            "unique-headings":false

    splitmarkdown:
      dev:
        expand:true
        cwd:"src/data"
        src:"**/*.md"
        dest:"texts"

    markdown:
      dev:
        expand:true
        src:"texts/*.md"
        dest:"texts/"
        ext:".html"
        options:
          markdownOptions:
            highlight: "manual"

    splicexml:
      dev:
        expand:true
        src:"texts/texts/*.html"
        dest:"bin/assets/data/text.xml"

    collatemedia:
      dev:
        src:"tmp/bin/assets/data/structure.json"
        dest:"tmp/bin/assets/media/"
        options:
          path:"assets/media/"
          extensions:[".mp4", ".webm", ".ogg", ".flv"]

    karma:
      dev:
        options:
          #baseUrl:"src"
          #basePath: "./"
          frameworks: [
            "mocha"
            "requirejs"
          ]
          files: [
            {pattern:"src/main-test.coffee", included:true}
            {pattern:"bin/assets/js/app/**/*.js", included:false}
            {pattern:"bin/assets/js/lib/**/*.js", included:false}
            {pattern:"bin/assets/data/**/*", served: true, included: false}
            # {pattern:"bin/assets/js/lib/moment/moment.js", included:false}
            # {pattern:"bin/assets/js/app/**/*.js", included:false}
          ]
          exclude: [
            'bin/assets/js/app/main.js'
          ]
          reporters: ["progress"]
          port: 9876
          colors: true
          autoWatch: true
          browsers: [
            #"Chrome"
            "PhantomJS"
          ]
          captureTimeout: 60000
          #singleRun: true

    protractor:
      options:
        keepAlive: true
        noColor: false
        configFile: "node_modules/protractor/referenceConf.js"
      dev:
        options:
          args:
            specs:
              ["e2e/**/#{spec}-spec.coffee"]

    selenium_webdriver_phantom:
      phantom:
        options:
          phantom:{}

    shell:
      protractor_webdriver_manager_update:
        options:
          stdout: true
        command: "node "+ require('path').resolve(__dirname, 'node_modules', 'protractor', 'bin', 'webdriver-manager') + ' update'



  #load plugins
  grunt.loadNpmTasks 'grunt-newer'
  grunt.loadNpmTasks 'grunt-accessibility'
  grunt.loadNpmTasks 'grunt-angular-templates'
  grunt.loadNpmTasks 'grunt-coffeelint'
  grunt.loadNpmTasks 'grunt-concurrent'
  grunt.loadNpmTasks 'grunt-contrib-clean'
  grunt.loadNpmTasks 'grunt-contrib-coffee'
  grunt.loadNpmTasks 'grunt-contrib-compress'
  grunt.loadNpmTasks 'grunt-contrib-copy'
  grunt.loadNpmTasks 'grunt-contrib-jade'
  grunt.loadNpmTasks 'grunt-contrib-less'
  grunt.loadNpmTasks 'grunt-contrib-requirejs'
  grunt.loadNpmTasks 'grunt-contrib-watch'
  grunt.loadNpmTasks 'grunt-html-angular-validate'
  grunt.loadNpmTasks 'grunt-lesslint'
  grunt.loadNpmTasks 'grunt-ngmin'
  grunt.loadNpmTasks 'grunt-rename'
  grunt.loadNpmTasks 'grunt-scorm-manifest'
  grunt.loadNpmTasks 'grunt-yaml'
  grunt.loadNpmTasks 'grunt-markdown'
  grunt.loadNpmTasks 'grunt-karma'
  grunt.loadNpmTasks 'grunt-protractor-runner'
  grunt.loadNpmTasks 'grunt-selenium-webdriver-phantom'
  grunt.loadNpmTasks 'grunt-shell'

  grunt.loadTasks( 'tasks' );

  #register tasks
  #compund tasks
  grunt.registerTask 'buildText', ['splitmarkdown:dev', 'markdown:dev', 'splicexml:dev', 'clean:texts']
  grunt.registerTask 'buildJade', ['jade:dev', 'rename:index', 'ngtemplates', 'clean:templates']
  grunt.registerTask 'buildJadeDeploy', ['jade:deploy', 'rename:index', 'ngtemplates', 'clean:templates']
  grunt.registerTask 'buildCoffee', ['coffee:dev', 'coffeelint']
  grunt.registerTask 'watchCoffee', ['newer:coffee:dev', 'coffeelint']
  grunt.registerTask 'buildLess', ['less:dev', 'lesslint']
  grunt.registerTask 'buildOnce', ['clean:app','buildLess', 'buildCoffee', 'buildJade', 'yaml:dev', 'buildText']
  grunt.registerTask 'buildOnceDeploy', ['less:deploy', 'buildCoffee:deploy', 'buildJadeDeploy', 'yaml:dev', 'buildText']
  
  #main tasks to run
  grunt.registerTask 'build', ['buildOnce', 'concurrent:watch']
  grunt.registerTask 'test', ['buildOnce', 'concurrent:test']
  #you can pass a specifc e2e spec to run with grunt e2e --spec init otherwise this runs all specs
  grunt.registerTask 'e2e', ['shell:protractor_webdriver_manager_update', 'buildOnce', 'protractor:dev']
  grunt.registerTask 'deploy', ['buildOnceDeploy', 'ngmin:dev', 'requirejs']
  grunt.registerTask 'validate', [
    'clean:reports'
    'buildCoffee'
    'watchCoffee'
    'buildLess'
    'jade:dev'
    'rename:index'
    'htmlangular'
    'accessibility'
    'clean:templates'
    'clean:reports'
  ]
  grunt.registerTask 'package', [
    'deploy'
    'copy:export'
    'clean:export'
    'copy:exportExceptions'
    'collatemedia'
    'compress:package'
  ]
  grunt.registerTask 'scorm', [
    'deploy'
    'copy:export'
    'clean:export'
    'copy:exportExceptions'
    'collatemedia'
    'scorm_manifest'
    'compress:scorm'
  ]
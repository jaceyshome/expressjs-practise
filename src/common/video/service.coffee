define [
  'angular'
  'signals'
  ], (angular, signals)->
  module = angular.module 'common.video.service', []
  module.factory 'Video', ($q, $rootScope)->
    service = {
      #properties
      currentTime:null
      bufferedPercent:null
      duration:null
      volume:null
      muted:null
      playing:null
      firstPlay:null
      ready:false

      #methods (overwritten by video directive)
      setSrc:->
      setCaptions:->
      play:->
      pause:->
      playPause:->
      mute:->
      unmute:->
      muteUnmute:->
      setVolume:->
      setWidth:->
      setHeight:->
      seek:->
      requestFullScreen:->
      cancelFullScreen:->
      setVisible:->
      setAspectRatio:->
      resetAspectRatio:->
      skip:->

      init:->
        deferred = $q.defer()
        $rootScope.$watch ->
          service.ready
        , (ready) ->
          deferred.resolve() if ready
        deferred.promise

      #signals
      loadstart: new signals.Signal
      loadedmetadata: new signals.Signal
      loadeddata: new signals.Signal
      loadedalldata: new signals.Signal
      onPlay: new signals.Signal
      onPause: new signals.Signal
      timeupdate: new signals.Signal
      ended: new signals.Signal
      durationchange: new signals.Signal
      progress: new signals.Signal
      resize: new signals.Signal
      volumechange: new signals.Signal
      error: new signals.Signal
      fullscreenchange: new signals.Signal
    }

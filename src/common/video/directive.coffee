define [
  'angular'
  'videojs'
  'common/video/service'
  'common/captions/main'
  ], ->
  module = angular.module 'common.video.directive', ['templates', 'common.captions']
  module.directive 'tntVideo', ($http)->
    restrict:"A"
    scope:
      control:"="
    templateUrl:"common/video/main"
    link:($scope, element, attrs) ->
      player = null
      preMutedVolume = null
      playPause = ->
        #console.log "play pause"
        if player.paused()
          play()
        else
          pause()

      play = ->
        #console.log "play"
        player.play()

      pause = ->
        #console.log "play"
        player.pause()

      setSrc = (val)->
        #console.log "tnt-video.src", val
        $scope.videoRatio = false
        if $scope.control
          $scope.control.firstPlay = true
          $scope.control.currentTime = 0
          $scope.control.bufferedPercent = 0
        player.src { type: "video/mp4", src: val }

      setCaptions = (val)->
        $scope.captions = val

      showCaptions = ()->
        $scope.control.captionsVisible = true if $scope.control?
        $scope.captionsVisible = true
      hideCaptions = ()->
        $scope.control.captionsVisible = false if $scope.control?
        $scope.captionsVisible = false


      muteUnmute = ->
        #console.log "mute unmute"
        if player.volume() is 0
          unmute()
        else
          mute()

      mute = ->
        #console.log "mute"
        preMutedVolume = player.volume()
        setVolume 0

      unmute = ->
        #console.log "unmute"
        player.volume preMutedVolume

      setVolume = (val)->
        #console.log "setVolume", val
        player.volume val

      seek = (time) ->
        #console.log "seek", time
        player.currentTime time

      skip = ->
        #console.log "skip"
        player.pause()
        $scope.control.ended.dispatch()


      requestFullScreen = ->
        player.requestFullScreen()

      cancelFullScreen = ->
        player.cancelFullScreen()

      setVisible = (val) ->
        #console.log "setVisible", val
        $scope.visible = val
        if player
          if val
            element.show()
            element.css('left','auto')
            resize()
          else
            element.css('left','-1000%')
            player.width 1
            player.height 1
            #flash player needs a clear function to stop last frame
            #carrying over
            if $scope.flash
              player.tech.el_.vjs_stop()

      useGivenDimensions = false
      proposedWidth = 1
      proposedHeight = 1
      proposedAspect = 1
      resetAspectRatio = ->
        useGivenDimensions = false
        proposedWidth = 1
        proposedHeight = 1
        proposedAspect = 1
      setAspectRatio = (w = 1, h = 1) ->
        # Aspect ratio only needs to be set if the browser is bugged
        # and can't get the video's width and height automatically
        # (ie native Android v4.1.2 browser)
        # console.log "Setting video aspect ratio to #{w}:#{h}"
        proposedWidth = w if !isNaN(parseInt(w)) and isFinite(w)
        proposedHeight = h if !isNaN(parseInt(h)) and isFinite(h)
        return unless proposedWidth? and proposedHeight?

        proposedAspect = proposedHeight / proposedWidth
        useGivenDimensions = true
        resize()
        # setVisible(true)

      resize = ->
        #console.log "resize", $scope.control.firstPlay
        if useGivenDimensions
          ratio = proposedAspect
        else
          ratio = $scope.videoRatio
        firstPlay = $scope.control.firstPlay
        if $scope.visible and not firstPlay and ratio
          #console.log "doing resize"
          #get width of container
          width = element.outerWidth()
          height = width*ratio
          # alert "RESIZE:\n\nWIDTH: #{width}\nHEIGHT: #{height}\nASPECT: #{$scope.videoRatio}"
          #set height according to ratio
          if player.width
            player.width width
            player.height height
          element.find("video").width width
          element.find("video").height height
          element.find(".videoPending").width width

      $scope.$watch "control", (val) ->
        #console.log "control", val
        $scope.control = val
        if val?
          val.setSrc = setSrc
          val.setCaptions = setCaptions
          val.showCaptions = showCaptions
          val.hideCaptions = hideCaptions
          val.play = play
          val.pause = pause
          val.playPause = playPause
          val.mute = mute
          val.unmute = unmute
          val.muteUnmute = muteUnmute
          val.seek = seek
          val.requestFullScreen = requestFullScreen
          val.cancelFullScreen = cancelFullScreen
          val.setVisible = setVisible
          val.setAspectRatio = setAspectRatio
          val.resetAspectRatio = resetAspectRatio
          val.skip = skip

      initVideo = ->
        #console.log "initVideo"
        setVisible(false)
        videojs.options.flash.swf = "assets/js/lib/video-js/video-js.swf"
        options = {
          techOrder: ["flash", "html5"]
        }
        videojs("videoPlayer", options).ready ->
          $scope.player = player = this
          #this.volume 0
          #console.log "player init", player
          $scope.flash = player.techName is "Flash"
          #console.log "$scope.flash", $scope.flash
          #console.log "window", window
          #console.log "$scope.visible", $scope.visible
          if !$scope.visible
            #fake hide the player
            player.width 1
            player.height 1

          player.on "loadstart", (e) ->
            #console.log "loadstart"
            if $scope.control?
              $scope.control.loadstart.dispatch()
              $scope.$apply() if !$scope.$root.$$phase?

          player.on "loadedmetadata", (e) ->
            #console.log "loadedmetadata", e, player
            #console.log "duration", player.duration()

            if $scope.flash
              #console.log "get flash size"
              $scope.videoWidth = player.techGet "videoWidth"
              $scope.videoHeight = player.techGet "videoHeight"
            else
              # Unfortunately, the native Android browser in v4.1.2
              # will never get the correct width/height (even with setTimeout)
              $scope.videoWidth = element.find("video").get(0).videoWidth
              $scope.videoHeight = element.find("video").get(0).videoHeight
              # alert "LOADMETA:\n\nWIDTH: #{$scope.videoWidth}\nHEIGHT: #{$scope.videoHeight}"

            #console.log "videoWidth", $scope.videoWidth
            #console.log "videoHeight", $scope.videoHeight
            $scope.videoRatio = $scope.videoHeight/$scope.videoWidth
            #console.log "videoRatio", $scope.videoRatio

            if $scope.control?
              $scope.control.duration = player.duration()
              $scope.control.width = player.width()
              $scope.control.height = player.height()
              $scope.control.loadedmetadata.dispatch()
              $scope.$apply() if !$scope.$root.$$phase?

            #console.log resize
            resize()
            setTimeout ->
              resize()
            , 0

          player.on "loadeddata", (e) ->
            #console.log "loadeddata", e
            #console.log player
            #console.log player.bufferedPercent()
            $scope.control.loadeddata.dispatch() if $scope.control?
            $scope.$apply() if !$scope.$root.$$phase?

          player.on "loadedalldata", (e) ->
            #console.log "loadedalldata"
            $scope.control.loadedalldata.dispatch() if $scope.control?
            $scope.$apply() if !$scope.$root.$$phase?

          player.on "play", (e) ->
            #console.log "play"
            if $scope.control?
              $scope.control.playing = true
              $scope.control.firstPlay = false
              resize()
              $scope.control.onPlay.dispatch()
              $scope.$apply() if !$scope.$root.$$phase?

          player.on "pause", (e) ->
            #console.log "pause"
            if $scope.control?
              $scope.control.playing = false
              $scope.control.onPause.dispatch()
              $scope.$apply() if !$scope.$root.$$phase?

          player.on "timeupdate", (e) ->
            #console.log "timeupdate", player.currentTime()
            if $scope.control?
              $scope.currentTime = player.currentTime()
              $scope.control.currentTime = $scope.currentTime
              $scope.control.timeupdate.dispatch $scope.currentTime
              $scope.$apply() if !$scope.$root.$$phase?

          player.on "ended", (e) ->
            #console.log "ended"
            if $scope.control?
              $scope.control.ended.dispatch()
              $scope.$apply() if !$scope.$root.$$phase?

          player.on "durationchange", (e) ->
            #console.log "durationchange", player.duration()
            if $scope.control?
              duration = player.duration()
              $scope.control.duration = duration
              $scope.control.durationchange.dispatch duration
              $scope.$apply() if !$scope.$root.$$phase?

          player.on "progress", (e) ->
            #console.log "progress", player.bufferedPercent()
            if $scope.control?
              #console.log player
              $scope.control.bufferedPercent = player.bufferedPercent()
              $scope.control.progress.dispatch player.bufferedPercent()
              $scope.$apply() if !$scope.$root.$$phase?

          player.on "resize", (e) ->
            #console.log "resize", e
            $scope.control.resize.dispatch() if $scope.control?
            $scope.$apply() if !$scope.$root.$$phase?

          player.on "volumechange", (e) ->
            #console.log "volumechange", player.volume()
            if $scope.control?
              $scope.control.volume = player.volume()
              $scope.control.muted = $scope.control.volume is 0
              $scope.control.volumechange.dispatch $scope.control.volume
              $scope.$apply() if !$scope.$root.$$phase?

          player.on "error", (e) ->
            # console.log "error", e
            $scope.control.error.dispatch() if $scope.control?
            $scope.$apply() if !$scope.$root.$$phase?

          player.on "fullscreenchange", (e) ->
            #console.log "fullscreenchange", e
            $scope.control.fullscreenchange.dispatch() if $scope.control?
            $scope.$apply() if !$scope.$root.$$phase?

          player.on "abort", (e) ->
            # console.log "abort", e
            # 'Fix' Mac OSX Safari aborting all videos when flash isn't installed
            plyr = document.getElementsByTagName("video")[0]
            if plyr?
              plyr.load()
              plyr.play()

          $scope.control.ready = true
          $scope.$apply() if !$scope.$root.$$phase?


      $(window).resize resize
      initVideo()


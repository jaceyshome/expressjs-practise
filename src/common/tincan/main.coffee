define [
  'angular'
  'scorm'
  ], ->
  module = angular.module 'common.tincan', []
  module.factory 'Scorm', ($q)->
    tincan = null
    ###
    @method init
    @param {Object} [options] Configuration used to initialize (see TinCan constructor).
    Phil: note you should generally only have to set url and activity like so:
    {
      url: window.location.href,
      activity: {
          id: ROOT_ACTIVITY_ID
      }
    }
    ###
    init:(cfg)->
      ###
      @class TinCan
      @constructor
      @param {Object} [options] Configuration used to initialize.
          @param {String} [options.url] URL for determining launch provided
              configuration options
          @param {Array} [options.recordStores] list of pre-configured LRSes
          @param {Object|TinCan.Agent} [options.actor] default actor
          @param {Object|TinCan.Activity} [options.activity] default activity
          @param {String} [options.registration] default registration
          @param {Object|TinCan.Context} [options.context] default context
      ###
      tincan = new TinCan(cfg)

    ###
    @method addRecordStore
    @param {Object} Configuration data

     * TODO:
     * check endpoint for trailing '/'
     * check for unique endpoints
    ###
    addRecordStore:(cfg)->
      tincan.addRecordStore(cfg)

    ###
    @method prepareStatement
    @param {Object|TinCan.Statement} Base statement properties or
        pre-created TinCan.Statement instance
    @return {TinCan.Statement}
    ###
    prepareStatement:(stmt)->
      tincan.prepareStatement(stmt)

    ###
    Calls saveStatement on each configured LRS, provide callback to make it asynchronous

    @method sendStatement
    @param {TinCan.Statement|Object} statement Send statement to LRS
    @param {Function} [callback] Callback function to execute on completion
    ###
    sendStatement:(stmt)->
      tincan.sendStatement(stmt, callback)

    ###
    Calls retrieveStatement on the first LRS, provide callback to make it asynchronous

    @method getStatement
    @param {String} statement Statement ID to get
    @param {Function} [callback] Callback function to execute on completion
    @return {Array|Result} Array of results, or single result

    TODO: make TinCan track statements it has seen in a local cache to be returned easily
    ###
    getStatement:(stmtId, callback)->
      tincan.getStatement(stmtId, callback)

    ###
    Creates a statement used for voiding the passed statement/statement ID and calls
    send statement with the voiding statement.

    @method voidStatement
    @param {TinCan.Statement|String} statement Statement or statement ID to void
    @param {Function} [callback] Callback function to execute on completion
    @param {Object} [options] Options used to build voiding statement
        @param {TinCan.Agent} [options.actor] Agent to be used as 'actor' in voiding statement
    ###
    voidStatement:(stmt, callback, options)->
      tincan.voidStatement(stmt, callback, options)

    ###
    Calls retrieveVoidedStatement on the first LRS, provide callback to make it asynchronous

    @method getVoidedStatement
    @param {String} statement Statement ID to get
    @param {Function} [callback] Callback function to execute on completion
    @return {Array|Result} Array of results, or single result

    TODO: make TinCan track voided statements it has seen in a local cache to be returned easily
    ###
    getVoidedStatement:(stmtId, callback)->
      tincan.getVoidedStatement(stmtId, callback)

    ###
    Calls saveStatements with list of prepared statements

    @method sendStatements
    @param {Array} Array of statements to send
    @param {Function} Callback function to execute on completion
    ###
    sendStatements:(stmts, callback)->
      tincan.sendStatements(stmts, callback)

    ###
    @method getStatements
    @param {Object} [cfg] Configuration for request
        @param {Boolean} [cfg.sendActor] Include default actor in query params
        @param {Boolean} [cfg.sendActivity] Include default activity in query params
        @param {Object} [cfg.params] Parameters used to filter

        @param {Function} [cfg.callback] Function to run at completion

    TODO: support multiple LRSs and flag to use single
    ###
    getStatements:(cfg)->
      tincan.getStatements(cfg)

    ###
    @method getState
    @param {String} key Key to retrieve from the state
    @param {Object} [cfg] Configuration for request
        @param {Object} [cfg.agent] Agent used in query,
            defaults to 'actor' property if empty
        @param {Object} [cfg.activity] Activity used in query,
            defaults to 'activity' property if empty
        @param {Object} [cfg.registration] Registration used in query,
            defaults to 'registration' property if empty
        @param {Function} [cfg.callback] Function to run with state
    ###
    getState:(key, cfg)->
      tincan.getState(key, cfg)

    ###
    @method setState
    @param {String} key Key to store into the state
    @param {String|Object} val Value to store into the state, objects will be stringified to JSON
    @param {Object} [cfg] Configuration for request
        @param {Object} [cfg.agent] Agent used in query,
            defaults to 'actor' property if empty
        @param {Object} [cfg.activity] Activity used in query,
            defaults to 'activity' property if empty
        @param {Object} [cfg.registration] Registration used in query,
            defaults to 'registration' property if empty
        @param {String} [cfg.lastSHA1] SHA1 of the previously seen existing state
        @param {String} [cfg.contentType] Content-Type to specify in headers
        @param {Function} [cfg.callback] Function to run with state
    ###
    setState:(key, val, cfg)->
      tincan.setState(key, val, cfg)

    ###
    @method deleteState
    @param {String|null} key Key to remove from the state, or null to clear all
    @param {Object} [cfg] Configuration for request
        @param {Object} [cfg.agent] Agent used in query,
            defaults to 'actor' property if empty
        @param {Object} [cfg.activity] Activity used in query,
            defaults to 'activity' property if empty
        @param {Object} [cfg.registration] Registration used in query,
            defaults to 'registration' property if empty
        @param {Function} [cfg.callback] Function to run with state
    ###
    deleteState:(key, cfg)->
      tincan.deleteState(key, cfg)

    ###
    @method getActivityProfile
    @param {String} key Key to retrieve from the profile
    @param {Object} [cfg] Configuration for request
        @param {Object} [cfg.activity] Activity used in query,
            defaults to 'activity' property if empty
        @param {Function} [cfg.callback] Function to run with activity profile
    ###
    getActivityProfile:(key, cfg)->
      tincan.getActivityProfile(key, cfg)

    ###
    @method setActivityProfile
    @param {String} key Key to store into the activity profile
    @param {String|Object} val Value to store into the activity profile, objects will be stringified to JSON
    @param {Object} [cfg] Configuration for request
        @param {Object} [cfg.activity] Activity used in query,
            defaults to 'activity' property if empty
        @param {String} [cfg.lastSHA1] SHA1 of the previously seen existing profile
        @param {String} [cfg.contentType] Content-Type to specify in headers
        @param {Function} [cfg.callback] Function to run with activity profile
    ###
    setActivityProfile:(key, val, cfg)->
      tincan.setActivityProfile(key, val, cfg)


    ###
    @method deleteActivityProfile
    @param {String|null} key Key to remove from the activity profile, or null to clear all
    @param {Object} [cfg] Configuration for request
        @param {Object} [cfg.activity] Activity used in query,
            defaults to 'activity' property if empty
        @param {Function} [cfg.callback] Function to run with activity profile
    ###
    deleteActivityProfile:(key, cfg)->
      tincan.deleteActivityProfile(key, cfg)

    ###
    @method getAgentProfile
    @param {String} key Key to retrieve from the profile
    @param {Object} [cfg] Configuration for request
        @param {Object} [cfg.agent] Agent used in query,
            defaults to 'actor' property if empty
        @param {Function} [cfg.callback] Function to run with agent profile
    ###
    getAgentProfile:(key, cfg)->
      tincan.getAgentProfile(key, cfg)

    ###
    @method setAgentProfile
    @param {String} key Key to store into the agent profile
    @param {String|Object} val Value to store into the agent profile, objects will be stringified to JSON
    @param {Object} [cfg] Configuration for request
        @param {Object} [cfg.agent] Agent used in query,
            defaults to 'actor' property if empty
        @param {String} [cfg.lastSHA1] SHA1 of the previously seen existing profile
        @param {String} [cfg.contentType] Content-Type to specify in headers
        @param {Function} [cfg.callback] Function to run with agent profile
    ###
    setAgentProfile:(key, val, cfg)->
      tincan.setAgentProfile(key, val, cfg)

    ###
    @method deleteAgentProfile
    @param {String|null} key Key to remove from the agent profile, or null to clear all
    @param {Object} [cfg] Configuration for request
        @param {Object} [cfg.agent] Agent used in query,
            defaults to 'actor' property if empty
        @param {Function} [cfg.callback] Function to run with agent profile
    ###
    deleteAgentProfile:(key, cfg)->
      tincan.deleteAgentProfile(key, cfg)
define [
  'angular'
  ], ->
  module = angular.module 'common.text.directive', []
  module.directive 'tntText', ($compile, Text) ->
    restrict:"A"
    scope:
      tntText:"="
    link:($scope, element, attrs) ->
      $scope.$watch "tntText", (val)->
        #console.log "tntText", val
        if val? and Text.texts
          if Text.texts[val]
            element.html Text.texts[val]
            $compile(element.contents())($scope)
          else
            console.log "Error: No text with id '" + val + "'"
            element.html val
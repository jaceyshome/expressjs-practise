define [
  'angular'
  'angular_resource'
  'common/scorm/main'
  'common/structure/main'
  ], ->
  module = angular.module 'common.bookmark', [
    'common.structure'
  ]
  module.factory 'Bookmark', (Scorm, Structure, $q) ->
    load: (callback) ->
      #console.log "Bookmark.load", structure
      deferred = $q.defer()
      Scorm.getBookmark (bookmark) ->
        if bookmark
          bookmark = JSON.parse bookmark
          Structure.data.complete = bookmark.complete is true
        deferred.resolve bookmark
      if callback
        deferred.promise.then callback
      deferred.promise

    save: (callback) ->
      deferred = $q.defer()
      bookmark = {
        complete:Structure.data.complete is true
      }
      bookmark = JSON.stringify bookmark
      # console.log "saving bookmark", bookmark

      Scorm.setBookmark bookmark, ->
        Scorm.save ->
          deferred.resolve

      if callback
        deferred.promise.then callback
      deferred.promise
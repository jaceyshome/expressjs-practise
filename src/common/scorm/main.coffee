define [
  'angular'
  ], ->
  pipwerks = window.pipwerks
  module = angular.module 'common.scorm', []
  module.factory 'Scorm', ($q)->
    service = {}
    service.init = (callback) ->
      deferred = $q.defer()
      #console.log "pipwerks", pipwerks
      scormAvailable = false
      if pipwerks?
        val = pipwerks.SCORM.init()
        val = null if val is "null" or val is "undefined"
        if val
          window.onbeforeunload = ->
            service.save()
            service.terminate()
            undefined
        else
          scormAvailable = true
      deferred.resolve scormAvailable
      if callback
        deferred.promise.then callback
      deferred.promise

    service.get = (key, callback) ->
      return callback false if pipwerks == undefined
      val = pipwerks.SCORM.get key
      val = null if val is "null" or val is "undefined"
      callback? val
      return val

    service.set = (key, value, callback) ->
      return callback false if pipwerks == undefined
      val = pipwerks.SCORM.set key, value
      val = null if val is "null" or val is "undefined"
      callback? val
      return val

    service.save = (callback) ->
      return callback false if pipwerks == undefined
      val = pipwerks.SCORM.save()
      callback? val
      return val

    service.terminate = (callback) ->
      return callback false if pipwerks == undefined
      val = pipwerks.SCORM.quit()
      callback? val
      return val

    service.getBookmark = (callback) ->
      return callback false if pipwerks == undefined
      service.get "cmi.suspend_data", callback

    service.setBookmark = (bookmark, callback) ->
      return callback false if pipwerks == undefined
      service.set "cmi.suspend_data", bookmark, callback

    service.getScore = (callback) ->
      return callback false if pipwerks == undefined
      service.get "cmi.core.score.raw", callback

    service.setScore = (bookmark, callback) ->
      return callback false if pipwerks == undefined
      service.set "cmi.core.score.raw", bookmark, callback

    service.getStatus = (callback) ->
      return callback false if pipwerks == undefined
      service.get "cmi.core.lesson_status", callback

    service.setStatus = (bookmark, callback) ->
      return callback false if pipwerks == undefined
      service.set "cmi.core.lesson_status", bookmark, callback

    return service
  
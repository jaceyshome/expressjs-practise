requirejs.config
  waitSeconds: 200
  urlArgs: "bust=" + (new Date()).getTime()
  paths:
    jquery: "../lib/jquery/jquery"
    bootstrap: "../lib/bootstrap/dist/js/bootstrap"
    angular: "../lib/angular/angular"
    angular_resource: "../lib/angular-resource/angular-resource"
    angular_ui_router: "../lib/angular-ui-router/index"
    angular_sanitize: "../lib/angular-sanitize/angular-sanitize"
    angular_animate: "../lib/angular-animate/angular-animate"
    xml2json: "../lib/xml2json/index"
    videojs: "../lib/video-js/video.dev"
    bowser: "../lib/bowser/bowser"
    tincan:"../lib/tincan/build/tincan"
    flash_detect:"../lib/flash_detect/index"
    signals:"../lib/signals/dist/signals"
    moment:"../lib/moment/moment"
  shim:
    angular:
      deps: ['jquery']
      exports: 'angular'
    bootstrap:
      deps: ['jquery']
      exports: 'bootstrap'
    angular_resource:
      deps: ['angular']
      exports: 'angular_resource'
    angular_ui_router:
      deps: ['angular']
      exports: 'angular_ui_router'
    angular_sanitize:
      deps: ['angular']
      exports: 'angular_sanitize'
    angular_animate:
      deps: ['angular']
      exports: 'angular_animate'

define [
  'angular'
  'bowser'
  'flash_detect'
  'app/main'
  ], (angular, bowser) ->
  return angular.element(document).ready ->
    #console.log "FlashDetect.installed", FlashDetect.installed
    #console.log "BrowserDetect.browser", BrowserDetect.browser
    #console.log "BrowserDetect.version", BrowserDetect.version
    browserSupported = !(bowser.msie && bowser.version < 8)
    needsFlash = bowser.firefox ||(bowser.msie && bowser.version == 8)
    hasFlash = FlashDetect.installed && FlashDetect.major >= 11
    #console.log "browserSupported", browserSupported
    #console.log "needsFlash", needsFlash
    #console.log "hasFlash", hasFlash
    canPlay = browserSupported && (!needsFlash || hasFlash)
    #console.log "canPlay", canPlay
    if canPlay
      #console.log "bootstrap angular app"
      return angular.bootstrap document, ['app']
    else
      #show cannot play error
      if !browserSupported
        alert "You are using a browser that this course doesn't support,
          for the best experience please upgrade to Google Chrome 22,
          Mozilla Firefox 17, Safari 5.1, Internet Explorer 9, or later."
      else
        alert "Flash player 11 or greater is required to see this content,
          for the best experience please install the Adobe Flash Player 11
          for your browser."
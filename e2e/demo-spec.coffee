describe "protractor demo", ->
  it "should show the angular website", ->
    browser.get "http://angularjs.org/"
    expect(element(By.css(".AngularJS-large")).getAttribute("src")).toBe "http://angularjs.org/img/AngularJS-large.png"
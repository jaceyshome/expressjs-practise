define(['angular'], function() {
  var module, pipwerks;
  pipwerks = window.pipwerks;
  module = angular.module('common.scorm', []);
  return module.factory('Scorm', function($q) {
    var service;
    service = {};
    service.init = function(callback) {
      var deferred, scormAvailable, val;
      deferred = $q.defer();
      scormAvailable = false;
      if (pipwerks != null) {
        val = pipwerks.SCORM.init();
        if (val === "null" || val === "undefined") {
          val = null;
        }
        if (val) {
          window.onbeforeunload = function() {
            service.save();
            service.terminate();
            return void 0;
          };
        } else {
          scormAvailable = true;
        }
      }
      deferred.resolve(scormAvailable);
      if (callback) {
        deferred.promise.then(callback);
      }
      return deferred.promise;
    };
    service.get = function(key, callback) {
      var val;
      if (pipwerks === void 0) {
        return callback(false);
      }
      val = pipwerks.SCORM.get(key);
      if (val === "null" || val === "undefined") {
        val = null;
      }
      if (typeof callback === "function") {
        callback(val);
      }
      return val;
    };
    service.set = function(key, value, callback) {
      var val;
      if (pipwerks === void 0) {
        return callback(false);
      }
      val = pipwerks.SCORM.set(key, value);
      if (val === "null" || val === "undefined") {
        val = null;
      }
      if (typeof callback === "function") {
        callback(val);
      }
      return val;
    };
    service.save = function(callback) {
      var val;
      if (pipwerks === void 0) {
        return callback(false);
      }
      val = pipwerks.SCORM.save();
      if (typeof callback === "function") {
        callback(val);
      }
      return val;
    };
    service.terminate = function(callback) {
      var val;
      if (pipwerks === void 0) {
        return callback(false);
      }
      val = pipwerks.SCORM.quit();
      if (typeof callback === "function") {
        callback(val);
      }
      return val;
    };
    service.getBookmark = function(callback) {
      if (pipwerks === void 0) {
        return callback(false);
      }
      return service.get("cmi.suspend_data", callback);
    };
    service.setBookmark = function(bookmark, callback) {
      if (pipwerks === void 0) {
        return callback(false);
      }
      return service.set("cmi.suspend_data", bookmark, callback);
    };
    service.getScore = function(callback) {
      if (pipwerks === void 0) {
        return callback(false);
      }
      return service.get("cmi.core.score.raw", callback);
    };
    service.setScore = function(bookmark, callback) {
      if (pipwerks === void 0) {
        return callback(false);
      }
      return service.set("cmi.core.score.raw", bookmark, callback);
    };
    service.getStatus = function(callback) {
      if (pipwerks === void 0) {
        return callback(false);
      }
      return service.get("cmi.core.lesson_status", callback);
    };
    service.setStatus = function(bookmark, callback) {
      if (pipwerks === void 0) {
        return callback(false);
      }
      return service.set("cmi.core.lesson_status", bookmark, callback);
    };
    return service;
  });
});

//# sourceMappingURL=main.js.map

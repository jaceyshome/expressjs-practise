define(['angular', 'videojs', 'common/video/main', 'common/timecode/main', 'common/mediaslider/main'], function() {
  var module;
  module = angular.module('common.videocontrols', ['templates', 'common.timecode', 'common.mediaslider', 'common.video.service']);
  return module.directive('tntVideoControls', function(Video) {
    return {
      restrict: "A",
      scope: {
        control: "="
      },
      templateUrl: "common/videocontrols/main",
      link: function($scope, element, attrs) {
        $scope.pause = function() {
          return Video.pause.apply(Video, arguments);
        };
        $scope.play = function() {
          return Video.play.apply(Video, arguments);
        };
        $scope.playPause = function() {
          return Video.playPause.apply(Video, arguments);
        };
        $scope.mute = function() {
          return Video.mute.apply(Video, arguments);
        };
        $scope.unmute = function() {
          return Video.unmute.apply(Video, arguments);
        };
        $scope.muteUnmute = function() {
          return Video.muteUnmute.apply(Video, arguments);
        };
        $scope.seek = function() {
          return Video.seek.apply(Video, arguments);
        };
        $scope.requestFullScreen = function() {
          return Video.requestFullScreen.apply(Video, arguments);
        };
        $scope.cancelFullScreen = function() {
          return Video.cancelFullScreen.apply(Video, arguments);
        };
        $scope.skip = function() {
          return Video.skip.apply(Video, arguments);
        };
        $scope.showCaptions = function() {
          return Video.showCaptions.apply(Video, arguments);
        };
        $scope.hideCaptions = function() {
          return Video.hideCaptions.apply(Video, arguments);
        };
        $scope.$watch((function() {
          return Video.playing;
        }), (function(val) {
          return $scope.playing = val;
        }), true);
        $scope.$watch((function() {
          return Video.muted;
        }), (function(val) {
          return $scope.muted = val;
        }), true);
        $scope.$watch((function() {
          return Video.currentTime;
        }), (function(val) {
          return $scope.currentTime = val;
        }), true);
        $scope.$watch((function() {
          return Video.duration;
        }), (function(val) {
          return $scope.duration = val;
        }), true);
        $scope.$watch((function() {
          return Video.bufferedPercent;
        }), (function(val) {
          return $scope.bufferedPercent = val;
        }), true);
        $scope.$watch((function() {
          return Video.firstPlay;
        }), (function(val) {
          return $scope.firstPlay = val;
        }), true);
        return $scope.$watch((function() {
          return Video.captionsVisible;
        }), (function(val) {
          return $scope.captionsVisible = val;
        }), true);
      }
    };
  });
});

//# sourceMappingURL=main.js.map

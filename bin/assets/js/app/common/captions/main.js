define(['angular', 'angular_sanitize'], function() {
  var module;
  module = angular.module('common.captions', ['templates', 'ngSanitize']);
  return module.directive('tntCaptions', function($http, $sanitize) {
    return {
      restrict: "A",
      scope: {
        src: "=",
        currentTime: "="
      },
      templateUrl: "common/captions/main",
      link: function($scope, element, attrs) {
        var captions, parseCueTime, trim, updateCaption;
        captions = null;
        $scope.$watch("src", function(val) {
          captions = null;
          $scope.caption = null;
          if (val != null) {
            return $http({
              method: 'GET',
              url: val,
              cache: true
            }).then(function(result) {
              var cue, i, id, j, line, lines, text, time;
              cue = void 0;
              time = void 0;
              text = void 0;
              lines = result.data.split("\n");
              line = "";
              id = void 0;
              i = 1;
              j = lines.length;
              captions = [];
              while (i < j) {
                line = vjs.trim(lines[i]);
                if (line) {
                  if (line.indexOf("-->") === -1) {
                    id = line;
                    line = vjs.trim(lines[++i]);
                  } else {
                    id = captions.length;
                  }
                  cue = {
                    id: id,
                    index: captions.length
                  };
                  time = line.split(" --> ");
                  cue.startTime = parseCueTime(time[0]);
                  cue.endTime = parseCueTime(time[1]);
                  text = [];
                  while (lines[++i] && (line = vjs.trim(lines[i]))) {
                    text.push(line);
                  }
                  cue.text = text.join("<br/>");
                  captions.push(cue);
                }
                i++;
              }
              return updateCaption();
            });
          }
        });
        parseCueTime = function(timeText) {
          var hours, minutes, ms, other, parts, seconds, time;
          parts = timeText.split(":");
          time = 0;
          hours = void 0;
          minutes = void 0;
          other = void 0;
          seconds = void 0;
          ms = void 0;
          if (parts.length === 3) {
            hours = parts[0];
            minutes = parts[1];
            other = parts[2];
          } else {
            hours = 0;
            minutes = parts[0];
            other = parts[1];
          }
          other = other.split(/\s+/);
          seconds = other.splice(0, 1)[0];
          seconds = seconds.split(/\.|,/);
          ms = parseFloat(seconds[1]);
          seconds = seconds[0];
          time += parseFloat(hours) * 3600;
          time += parseFloat(minutes) * 60;
          time += parseFloat(seconds);
          if (ms) {
            time += ms / 1000;
          }
          return time;
        };
        trim = function(str) {
          return str.toString().replace(/^\s+/, '').replace(/\s+$/, '');
        };
        $scope.$watch("currentTime", function(val) {
          return updateCaption();
        });
        return updateCaption = function() {
          var caption, _i, _len, _results;
          if (($scope.currentTime != null) && (captions != null)) {
            $scope.caption = null;
            _results = [];
            for (_i = 0, _len = captions.length; _i < _len; _i++) {
              caption = captions[_i];
              if (caption.startTime <= $scope.currentTime && caption.endTime >= $scope.currentTime) {
                $scope.caption = caption.text;
                break;
              } else {
                _results.push(void 0);
              }
            }
            return _results;
          }
        };
      }
    };
  });
});

//# sourceMappingURL=main.js.map

define(['angular', 'angular_resource', 'common/scorm/main', 'common/structure/main'], function() {
  var module;
  module = angular.module('common.bookmark', ['common.structure']);
  return module.factory('Bookmark', function(Scorm, Structure, $q) {
    return {
      load: function(callback) {
        var deferred;
        deferred = $q.defer();
        Scorm.getBookmark(function(bookmark) {
          if (bookmark) {
            bookmark = JSON.parse(bookmark);
            Structure.data.complete = bookmark.complete === true;
          }
          return deferred.resolve(bookmark);
        });
        if (callback) {
          deferred.promise.then(callback);
        }
        return deferred.promise;
      },
      save: function(callback) {
        var bookmark, deferred;
        deferred = $q.defer();
        bookmark = {
          complete: Structure.data.complete === true
        };
        bookmark = JSON.stringify(bookmark);
        Scorm.setBookmark(bookmark, function() {
          return Scorm.save(function() {
            return deferred.resolve;
          });
        });
        if (callback) {
          deferred.promise.then(callback);
        }
        return deferred.promise;
      }
    };
  });
});

//# sourceMappingURL=main.js.map

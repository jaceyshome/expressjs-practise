define(['angular'], function() {
  var module;
  module = angular.module('common.autofillfix', []);
  return module.directive('tntAutofillFix', function() {
    return {
      restrict: "A",
      scope: {},
      link: function($scope, element, attrs) {
        var monitorId;
        monitorId = window.setInterval(function() {
          element.find('input').trigger('input');
          return element.find('input').trigger('change');
        }, 100);
        return $scope.$on("$destroy", function() {
          return window.clearInterval(monitorId);
        });
      }
    };
  });
});

//# sourceMappingURL=main.js.map

define(['angular'], function() {
  var module;
  module = angular.module('common.mediaslider', ['templates']);
  return module.directive('tntMediaSlider', function() {
    return {
      restrict: "A",
      scope: {
        currentTime: "=",
        duration: "=",
        bufferedPercent: "=",
        seek: "="
      },
      templateUrl: "common/mediaslider/main",
      link: function($scope, element, attrs) {
        var updateBars;
        updateBars = function() {
          $scope.playDisplayPercent = 0;
          $scope.bufferDisplayPercent = 0;
          if (($scope.currentTime != null) && ($scope.duration != null)) {
            $scope.playDisplayPercent = 100 * $scope.currentTime / $scope.duration;
            return $scope.bufferDisplayPercent = (100 * $scope.bufferedPercent) - $scope.playDisplayPercent;
          }
        };
        $scope.$watch("currentTime", function(val) {
          $scope.currentTime = val;
          return updateBars();
        });
        $scope.$watch("duration", function(val) {
          $scope.duration = val;
          return updateBars();
        });
        $scope.$watch("bufferedPercent", function(val) {
          return updateBars();
        });
        element.click(function(e) {
          var position, time;
          position = e.pageX - element.offset().left;
          time = $scope.duration * position / element.outerWidth();
          return typeof $scope.seek === "function" ? $scope.seek(time) : void 0;
        });
        return updateBars();
      }
    };
  });
});

//# sourceMappingURL=main.js.map

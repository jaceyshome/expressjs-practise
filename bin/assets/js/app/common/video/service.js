define(['angular', 'signals'], function(angular, signals) {
  var module;
  module = angular.module('common.video.service', []);
  return module.factory('Video', function($q, $rootScope) {
    var service;
    return service = {
      currentTime: null,
      bufferedPercent: null,
      duration: null,
      volume: null,
      muted: null,
      playing: null,
      firstPlay: null,
      ready: false,
      setSrc: function() {},
      setCaptions: function() {},
      play: function() {},
      pause: function() {},
      playPause: function() {},
      mute: function() {},
      unmute: function() {},
      muteUnmute: function() {},
      setVolume: function() {},
      setWidth: function() {},
      setHeight: function() {},
      seek: function() {},
      requestFullScreen: function() {},
      cancelFullScreen: function() {},
      setVisible: function() {},
      setAspectRatio: function() {},
      resetAspectRatio: function() {},
      skip: function() {},
      init: function() {
        var deferred;
        deferred = $q.defer();
        $rootScope.$watch(function() {
          return service.ready;
        }, function(ready) {
          if (ready) {
            return deferred.resolve();
          }
        });
        return deferred.promise;
      },
      loadstart: new signals.Signal,
      loadedmetadata: new signals.Signal,
      loadeddata: new signals.Signal,
      loadedalldata: new signals.Signal,
      onPlay: new signals.Signal,
      onPause: new signals.Signal,
      timeupdate: new signals.Signal,
      ended: new signals.Signal,
      durationchange: new signals.Signal,
      progress: new signals.Signal,
      resize: new signals.Signal,
      volumechange: new signals.Signal,
      error: new signals.Signal,
      fullscreenchange: new signals.Signal
    };
  });
});

//# sourceMappingURL=service.js.map

define(['angular', 'videojs', 'common/video/service', 'common/captions/main'], function() {
  var module;
  module = angular.module('common.video.directive', ['templates', 'common.captions']);
  return module.directive('tntVideo', function($http) {
    return {
      restrict: "A",
      scope: {
        control: "="
      },
      templateUrl: "common/video/main",
      link: function($scope, element, attrs) {
        var cancelFullScreen, hideCaptions, initVideo, mute, muteUnmute, pause, play, playPause, player, preMutedVolume, proposedAspect, proposedHeight, proposedWidth, requestFullScreen, resetAspectRatio, resize, seek, setAspectRatio, setCaptions, setSrc, setVisible, setVolume, showCaptions, skip, unmute, useGivenDimensions;
        player = null;
        preMutedVolume = null;
        playPause = function() {
          if (player.paused()) {
            return play();
          } else {
            return pause();
          }
        };
        play = function() {
          return player.play();
        };
        pause = function() {
          return player.pause();
        };
        setSrc = function(val) {
          $scope.videoRatio = false;
          if ($scope.control) {
            $scope.control.firstPlay = true;
            $scope.control.currentTime = 0;
            $scope.control.bufferedPercent = 0;
          }
          return player.src({
            type: "video/mp4",
            src: val
          });
        };
        setCaptions = function(val) {
          return $scope.captions = val;
        };
        showCaptions = function() {
          if ($scope.control != null) {
            $scope.control.captionsVisible = true;
          }
          return $scope.captionsVisible = true;
        };
        hideCaptions = function() {
          if ($scope.control != null) {
            $scope.control.captionsVisible = false;
          }
          return $scope.captionsVisible = false;
        };
        muteUnmute = function() {
          if (player.volume() === 0) {
            return unmute();
          } else {
            return mute();
          }
        };
        mute = function() {
          preMutedVolume = player.volume();
          return setVolume(0);
        };
        unmute = function() {
          return player.volume(preMutedVolume);
        };
        setVolume = function(val) {
          return player.volume(val);
        };
        seek = function(time) {
          return player.currentTime(time);
        };
        skip = function() {
          player.pause();
          return $scope.control.ended.dispatch();
        };
        requestFullScreen = function() {
          return player.requestFullScreen();
        };
        cancelFullScreen = function() {
          return player.cancelFullScreen();
        };
        setVisible = function(val) {
          $scope.visible = val;
          if (player) {
            if (val) {
              element.show();
              element.css('left', 'auto');
              return resize();
            } else {
              element.css('left', '-1000%');
              player.width(1);
              player.height(1);
              if ($scope.flash) {
                return player.tech.el_.vjs_stop();
              }
            }
          }
        };
        useGivenDimensions = false;
        proposedWidth = 1;
        proposedHeight = 1;
        proposedAspect = 1;
        resetAspectRatio = function() {
          useGivenDimensions = false;
          proposedWidth = 1;
          proposedHeight = 1;
          return proposedAspect = 1;
        };
        setAspectRatio = function(w, h) {
          if (w == null) {
            w = 1;
          }
          if (h == null) {
            h = 1;
          }
          if (!isNaN(parseInt(w)) && isFinite(w)) {
            proposedWidth = w;
          }
          if (!isNaN(parseInt(h)) && isFinite(h)) {
            proposedHeight = h;
          }
          if (!((proposedWidth != null) && (proposedHeight != null))) {
            return;
          }
          proposedAspect = proposedHeight / proposedWidth;
          useGivenDimensions = true;
          return resize();
        };
        resize = function() {
          var firstPlay, height, ratio, width;
          if (useGivenDimensions) {
            ratio = proposedAspect;
          } else {
            ratio = $scope.videoRatio;
          }
          firstPlay = $scope.control.firstPlay;
          if ($scope.visible && !firstPlay && ratio) {
            width = element.outerWidth();
            height = width * ratio;
            if (player.width) {
              player.width(width);
              player.height(height);
            }
            element.find("video").width(width);
            element.find("video").height(height);
            return element.find(".videoPending").width(width);
          }
        };
        $scope.$watch("control", function(val) {
          $scope.control = val;
          if (val != null) {
            val.setSrc = setSrc;
            val.setCaptions = setCaptions;
            val.showCaptions = showCaptions;
            val.hideCaptions = hideCaptions;
            val.play = play;
            val.pause = pause;
            val.playPause = playPause;
            val.mute = mute;
            val.unmute = unmute;
            val.muteUnmute = muteUnmute;
            val.seek = seek;
            val.requestFullScreen = requestFullScreen;
            val.cancelFullScreen = cancelFullScreen;
            val.setVisible = setVisible;
            val.setAspectRatio = setAspectRatio;
            val.resetAspectRatio = resetAspectRatio;
            return val.skip = skip;
          }
        });
        initVideo = function() {
          var options;
          setVisible(false);
          videojs.options.flash.swf = "assets/js/lib/video-js/video-js.swf";
          options = {
            techOrder: ["flash", "html5"]
          };
          return videojs("videoPlayer", options).ready(function() {
            $scope.player = player = this;
            $scope.flash = player.techName === "Flash";
            if (!$scope.visible) {
              player.width(1);
              player.height(1);
            }
            player.on("loadstart", function(e) {
              if ($scope.control != null) {
                $scope.control.loadstart.dispatch();
                if ($scope.$root.$$phase == null) {
                  return $scope.$apply();
                }
              }
            });
            player.on("loadedmetadata", function(e) {
              if ($scope.flash) {
                $scope.videoWidth = player.techGet("videoWidth");
                $scope.videoHeight = player.techGet("videoHeight");
              } else {
                $scope.videoWidth = element.find("video").get(0).videoWidth;
                $scope.videoHeight = element.find("video").get(0).videoHeight;
              }
              $scope.videoRatio = $scope.videoHeight / $scope.videoWidth;
              if ($scope.control != null) {
                $scope.control.duration = player.duration();
                $scope.control.width = player.width();
                $scope.control.height = player.height();
                $scope.control.loadedmetadata.dispatch();
                if ($scope.$root.$$phase == null) {
                  $scope.$apply();
                }
              }
              resize();
              return setTimeout(function() {
                return resize();
              }, 0);
            });
            player.on("loadeddata", function(e) {
              if ($scope.control != null) {
                $scope.control.loadeddata.dispatch();
              }
              if ($scope.$root.$$phase == null) {
                return $scope.$apply();
              }
            });
            player.on("loadedalldata", function(e) {
              if ($scope.control != null) {
                $scope.control.loadedalldata.dispatch();
              }
              if ($scope.$root.$$phase == null) {
                return $scope.$apply();
              }
            });
            player.on("play", function(e) {
              if ($scope.control != null) {
                $scope.control.playing = true;
                $scope.control.firstPlay = false;
                resize();
                $scope.control.onPlay.dispatch();
                if ($scope.$root.$$phase == null) {
                  return $scope.$apply();
                }
              }
            });
            player.on("pause", function(e) {
              if ($scope.control != null) {
                $scope.control.playing = false;
                $scope.control.onPause.dispatch();
                if ($scope.$root.$$phase == null) {
                  return $scope.$apply();
                }
              }
            });
            player.on("timeupdate", function(e) {
              if ($scope.control != null) {
                $scope.currentTime = player.currentTime();
                $scope.control.currentTime = $scope.currentTime;
                $scope.control.timeupdate.dispatch($scope.currentTime);
                if ($scope.$root.$$phase == null) {
                  return $scope.$apply();
                }
              }
            });
            player.on("ended", function(e) {
              if ($scope.control != null) {
                $scope.control.ended.dispatch();
                if ($scope.$root.$$phase == null) {
                  return $scope.$apply();
                }
              }
            });
            player.on("durationchange", function(e) {
              var duration;
              if ($scope.control != null) {
                duration = player.duration();
                $scope.control.duration = duration;
                $scope.control.durationchange.dispatch(duration);
                if ($scope.$root.$$phase == null) {
                  return $scope.$apply();
                }
              }
            });
            player.on("progress", function(e) {
              if ($scope.control != null) {
                $scope.control.bufferedPercent = player.bufferedPercent();
                $scope.control.progress.dispatch(player.bufferedPercent());
                if ($scope.$root.$$phase == null) {
                  return $scope.$apply();
                }
              }
            });
            player.on("resize", function(e) {
              if ($scope.control != null) {
                $scope.control.resize.dispatch();
              }
              if ($scope.$root.$$phase == null) {
                return $scope.$apply();
              }
            });
            player.on("volumechange", function(e) {
              if ($scope.control != null) {
                $scope.control.volume = player.volume();
                $scope.control.muted = $scope.control.volume === 0;
                $scope.control.volumechange.dispatch($scope.control.volume);
                if ($scope.$root.$$phase == null) {
                  return $scope.$apply();
                }
              }
            });
            player.on("error", function(e) {
              if ($scope.control != null) {
                $scope.control.error.dispatch();
              }
              if ($scope.$root.$$phase == null) {
                return $scope.$apply();
              }
            });
            player.on("fullscreenchange", function(e) {
              if ($scope.control != null) {
                $scope.control.fullscreenchange.dispatch();
              }
              if ($scope.$root.$$phase == null) {
                return $scope.$apply();
              }
            });
            player.on("abort", function(e) {
              var plyr;
              plyr = document.getElementsByTagName("video")[0];
              if (plyr != null) {
                plyr.load();
                return plyr.play();
              }
            });
            $scope.control.ready = true;
            if ($scope.$root.$$phase == null) {
              return $scope.$apply();
            }
          });
        };
        $(window).resize(resize);
        return initVideo();
      }
    };
  });
});

//# sourceMappingURL=directive.js.map

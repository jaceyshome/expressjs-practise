define(['angular'], function() {
  var module;
  module = angular.module('common.text.directive', []);
  return module.directive('tntText', function($compile, Text) {
    return {
      restrict: "A",
      scope: {
        tntText: "="
      },
      link: function($scope, element, attrs) {
        return $scope.$watch("tntText", function(val) {
          if ((val != null) && Text.texts) {
            if (Text.texts[val]) {
              element.html(Text.texts[val]);
              return $compile(element.contents())($scope);
            } else {
              console.log("Error: No text with id '" + val + "'");
              return element.html(val);
            }
          }
        });
      }
    };
  });
});

//# sourceMappingURL=directive.js.map

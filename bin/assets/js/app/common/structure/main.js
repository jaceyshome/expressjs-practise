define(['angular'], function(angular, jsyaml) {
  var module;
  module = angular.module('common.structure', []);
  return module.factory('Structure', function($http, $q) {
    var service;
    service = {};
    service.load = function(callback) {
      var deferred;
      console.log("loading");
      deferred = $q.defer();
      $http.get('assets/data/structure.json').success(function(data, status) {
        console.log("load success");
        service.data = data;
        return deferred.resolve(data);
      });
      if (callback) {
        deferred.promise.then(callback);
      }
      return deferred.promise;
    };
    return service;
  });
});

//# sourceMappingURL=main.js.map

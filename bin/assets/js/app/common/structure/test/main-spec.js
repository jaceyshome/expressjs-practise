define(["should", "angular_mocks", "common/structure/main"], function() {
  return describe('service', function() {
    beforeEach(angular.mock.module("common.structure"));
    return describe("structure", function() {
      it('should have a structure service', inject(function(Structure) {
        return Structure.should.not.equal(null);
      }));
      return it('should have a load function', inject(function(Structure) {
        return Structure.load.should.not.equal(null);
      }));

      /*
      it 'should retreive structure data', (done)->
        inject (Structure)->
          Structure.load().then (data)->
            console.log "data", data
            done()
       */
    });
  });
});

//# sourceMappingURL=main-spec.js.map

define(['angular', 'angular_resource'], function() {
  var module;
  module = angular.module('common.screen', []);
  return module.factory('Screen', function(Structure, $state, $rootScope) {
    var backScreen, checkIfBackScreen, checkIfNextScreen, checkIfPreviousScreen, getScreenList, service;
    service = {};
    $rootScope.$watch(function() {
      if (Structure.data != null) {
        return Structure.data.screen;
      }
    }, function(screen) {
      return service.screen = screen;
    });
    backScreen = void 0;
    service.go = function(id) {
      var lastState, screen, _i, _len, _ref;
      lastState = false;
      if (service.screen) {
        lastState = service.screen.state;
        backScreen = service.screen.id;
      }
      _ref = Structure.data.screens;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        screen = _ref[_i];
        if (screen.id === id) {
          Structure.data.screen = screen;
        }
      }
      service.screen = Structure.data.screen;
      if (lastState && lastState === service.screen.state) {
        return $state.reload();
      } else {
        return $state.go(Structure.data.screen.state, id);
      }
    };
    service.first = function() {
      var screen;
      if (Structure.data.screen == null) {
        screen = Structure.data.screens[0];
      }
      return service.go(screen.id);
    };
    getScreenList = function() {
      var item, list, _i, _len, _ref;
      list = [];
      _ref = Structure.data.screens;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        item = _ref[_i];
        if (item.hidden !== true) {
          list.push(item);
        }
      }
      return list;
    };
    service.getScreens = function() {
      return getScreenList();
    };
    checkIfNextScreen = function() {
      var index, list, _ref;
      if ((Structure != null ? (_ref = Structure.data) != null ? _ref.screens : void 0 : void 0) == null) {
        return false;
      }
      list = getScreenList();
      index = list.indexOf(Structure.data.screen);
      if (index !== -1) {
        index++;
        if (index < list.length) {
          return index;
        }
      }
      return -1;
    };
    service.hasNext = function() {
      if (checkIfNextScreen() >= 0) {
        return true;
      } else {
        return false;
      }
    };
    service.next = function() {
      var index, list;
      index = checkIfNextScreen();
      if (index >= 0) {
        list = getScreenList();
        return service.go(list[index].id);
      } else {
        return console.log("No next screen.");
      }
    };
    checkIfPreviousScreen = function() {
      var index, list, _ref;
      if ((Structure != null ? (_ref = Structure.data) != null ? _ref.screens : void 0 : void 0) == null) {
        return false;
      }
      list = getScreenList();
      index = list.indexOf(Structure.data.screen);
      if (index !== -1) {
        index--;
        if (index >= 0) {
          return index;
        }
      }
      return -1;
    };
    service.hasPrevious = function() {
      if (checkIfPreviousScreen() >= 0) {
        return true;
      } else {
        return false;
      }
    };
    service.previous = function() {
      var index, list;
      index = checkIfPreviousScreen();
      if (index >= 0) {
        list = getScreenList();
        return service.go(list[index].id);
      } else {
        return console.log("No previous screen.");
      }
    };
    checkIfBackScreen = function() {
      var _ref;
      if ((Structure != null ? (_ref = Structure.data) != null ? _ref.screens : void 0 : void 0) == null) {
        return false;
      }
      if (backScreen != null) {
        return backScreen;
      }
      return -1;
    };
    service.hasBack = function() {
      if (checkIfBackScreen() !== -1) {
        return true;
      } else {
        return false;
      }
    };
    service.back = function() {
      var id;
      id = checkIfBackScreen();
      if (id !== -1) {
        return service.go(id);
      } else {
        return console.log("No last viewed screen.");
      }
    };
    service.getScreenData = function(id) {};
    return service;
  });
});

//# sourceMappingURL=main.js.map

define(["should", "angular_mocks", "common/timecode/main"], function() {
  return describe('filter', function() {
    beforeEach(angular.mock.module("common.timecode"));
    return describe("timecode", function() {
      it('should have a timecode filter', inject(function($filter) {
        return ($filter("timecode")).should.not.equal(null);
      }));
      return it("should return 0 as 00:00:00", inject(function($filter) {
        return ($filter("timecode")(0)).should.equal("00:00:00");
      }));
    });
  });
});

//# sourceMappingURL=main-spec.js.map

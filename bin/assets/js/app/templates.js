define(['angular'], function() {angular.module('templates', []).run([                '$templateCache',                 function($templateCache) {  'use strict';

  $templateCache.put('app/main',
    "<div data-ng-controller=MainCtrl><div data-ng-hide=true class=container><h2>Initialising application</h2><div class=\"progress progress-striped active\"><div style=width:100% class=progress-bar></div></div></div><div data-ng-cloak=data-ng-cloak class=container><div data-ng-hide=ready><h2>Loading data</h2><div class=\"progress progress-striped active\"><div style=width:100% class=progress-bar></div></div></div><div data-ng-show=ready class=initialiseWrapper><header><h1 class=title>2and2 project</h1><div data-tnt-navigation=data-tnt-navigation></div></header><div data-ui-view=data-ui-view></div></div><div class=videoContainer><div data-tnt-video=data-tnt-video data-control=Video class=videoPlayer></div></div></div></div>"
  );


  $templateCache.put('app/states/hidden/main',
    "<div><h2>Hidden screen</h2><p data-tnt-text=screen.data.body></p><p><a href=JavaScript:void(0); data-ng-show=Screen.hasBack() data-ng-click=Screen.back()>Go back</a></p></div>"
  );


  $templateCache.put('app/states/intro/main',
    "<div data-ng-controller=IntroCtrl><h2>Module intro</h2><p>Here is the text</p><p><a href=JavaScript:void(0); data-ng-click=Screen.next()>Next</a></p></div>"
  );


  $templateCache.put('app/states/text/main',
    "<div><h2>Text screen</h2><p data-tnt-text=screen.data.body></p><p><a href=JavaScript:void(0); data-ng-click=Screen.next()>Next</a></p></div>"
  );


  $templateCache.put('app/states/video/main',
    "<div><h2>Video screen</h2><p>Here is the text</p><p data-ng-show=ended><a href=JavaScript:void(0); data-ng-click=Screen.next()>Next</a></p><div data-tnt-video-controls=data-tnt-video-controls></div></div>"
  );


  $templateCache.put('common/captions/main',
    "<p data-ng-show=caption data-ng-bind-html=caption class=caption></p>"
  );


  $templateCache.put('common/mediaslider/main',
    "<div class=\"mediaSlider progress\"><div style=\"width: {{playDisplayPercent}}%\" class=\"bufferBar progress-bar\"></div><div style=\"width: {{bufferDisplayPercent}}%\" class=\"playBar progress-bar progress-bar-success\"></div></div>"
  );


  $templateCache.put('common/navigation/main',
    "<ul class=\"nav nav-pills\"><li data-ng-class=\"{active:screen.id=='intro'}\"><a href=JavaScript:void(0); data-ng-click=\"Screen.go('intro')\">Intro</a></li><li data-ng-class=\"{active:screen.id=='video1'}\"><a href=JavaScript:void(0); data-ng-click=\"Screen.go('video1')\">Video 1</a></li><li data-ng-class=\"{active:screen.id=='text1'}\"><a href=JavaScript:void(0); data-ng-click=\"Screen.go('text1')\">Text 1</a></li><li data-ng-class=\"{active:screen.id=='hidden1'}\"><a href=JavaScript:void(0); data-ng-click=\"Screen.go('hidden1')\">Hidden 1</a></li><li data-ng-class=\"{active:screen.id=='video2'}\"><a href=JavaScript:void(0); data-ng-click=\"Screen.go('video2')\">Video 2</a></li><li data-ng-class=\"{active:screen.id=='text2'}\"><a href=JavaScript:void(0); data-ng-click=\"Screen.go('text2')\">Text 2</a></li></ul>"
  );


  $templateCache.put('common/pagination/main',
    "<ul class=pagination><li data-ng-class=\"{disabled:index == 0 || displayPages.length == 0}\"><a href=JavaScript:void(0); data-ng-click=first()>&laquo;</a></li><li data-ng-class=\"{disabled:index == 0 || displayPages.length == 0}\"><a href=JavaScript:void(0); data-ng-click=previous()>&lt;</a></li><li data-ng-repeat=\"page in displayPages\" data-ng-class={active:page.current}><a href=JavaScript:void(0); data-ng-click=goPage(page)>{{page.index+1}}</a></li><li data-ng-class=\"{disabled:index &gt;= lastPage.first || displayPages.length == 0}\"><a href=JavaScript:void(0); data-ng-click=next()>&gt;</a></li><li data-ng-class=\"{disabled:index &gt;= lastPage.first || displayPages.length == 0}\"><a href=JavaScript:void(0); data-ng-click=last()>&raquo;</a></li></ul>"
  );


  $templateCache.put('common/video/main',
    "<div data-ng-hide=!videoPending class=videoPending><div class=spinner></div></div><video id=videoPlayer controls=controls preload=metadata class=\"video-js vjs-default-skin vjs-big-play-centered\"></video><div data-tnt-captions=data-tnt-captions data-src=captions data-ng-show=captionsVisible data-current-time=currentTime class=captions></div>"
  );


  $templateCache.put('common/videocontrols/main',
    "<div class=videoControls><div class=btn-group><a href=JavaScript:void(0); data-ng-click=play() data-ng-hide=playing class=\"btn btn-default\">Play</a><a href=JavaScript:void(0); data-ng-click=play() data-ng-show=firstPlay class=\"btn btn-default\">Big play button</a><a href=JavaScript:void(0); data-ng-click=pause() data-ng-show=playing class=\"btn btn-default\">Pause</a><a href=JavaScript:void(0); data-ng-click=seek(0) class=\"btn btn-default\">Rewind</a><a href=JavaScript:void(0); data-ng-click=mute() data-ng-hide=muted class=\"btn btn-default\">Mute</a><a href=JavaScript:void(0); data-ng-click=unmute() data-ng-show=muted class=\"btn btn-default\">Unmute</a><a href=JavaScript:void(0); data-ng-click=requestFullScreen() data-ng-hide=isFullscreen class=\"btn btn-default\">Fullscreen</a><a href=JavaScript:void(0); data-ng-click=skip() class=\"btn btn-default\">Skip video</a><a href=JavaScript:void(0); data-ng-click=showCaptions() data-ng-hide=captionsVisible class=\"btn btn-default\">Show captions</a><a href=JavaScript:void(0); data-ng-click=hideCaptions() data-ng-show=captionsVisible class=\"btn btn-default\">Hide captions</a></div><div data-tnt-media-slider=data-tnt-media-slider data-current-time=currentTime data-duration=duration data-buffered-percent=bufferedPercent data-seek=seek></div><span>{{currentTime*1000 | timecode}}/{{duration*1000 | timecode}}</span></div>"
  );
}              ]);});
define(['angular'], function() {
  var module;
  module = angular.module('app.states.hidden', []);
  return module.controller('HiddenCtrl', function($scope, Screen) {
    $scope.Screen = Screen;
    return $scope.$watch(function() {
      return Screen.screen;
    }, function() {
      return $scope.screen = Screen.screen;
    });
  });
});

//# sourceMappingURL=main.js.map

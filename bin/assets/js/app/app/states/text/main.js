define(['angular'], function() {
  var module;
  module = angular.module('app.states.text', []);
  return module.controller('TextCtrl', function($scope, Screen) {
    $scope.Screen = Screen;
    return $scope.$watch(function() {
      return Screen.screen;
    }, function() {
      return $scope.screen = Screen.screen;
    });
  });
});

//# sourceMappingURL=main.js.map

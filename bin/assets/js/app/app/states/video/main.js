define(['angular', 'common/video/main', 'common/videocontrols/main', 'common/timecode/main'], function() {
  var module;
  module = angular.module('app.states.video', ['common.videocontrols']);
  return module.controller('VideoCtrl', function($scope, Video, Screen) {
    $scope.Screen = Screen;
    return $scope.$watch(function() {
      return Screen.screen;
    }, function() {
      $scope.screen = Screen.screen;
      $scope.ended = false;
      return Video.ended.addOnce(function() {
        return $scope.ended = true;
      });
    });
  });
});

//# sourceMappingURL=main.js.map

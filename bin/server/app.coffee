http = require 'http'
express = require 'express'
app = express()
app.use(express.responseTime())
app.use(app.router)
app.use(express.errorHandler())
app.use(express.logger('dev'))

app.set('view engine', 'jade')
app.set('views', './src')

#routes
app.get('/', (req,res)->
  res.render('main',{title:"hello world", message:"jake and stacey"})
  console.log "app.get(env)", app.get('env') #to get the app development environment
)

app.use(express.static('./assets'))
#app.use(express.static('./files'))
#app.use(express.static('./downloads'))

http.createServer(app).listen(3000, ->
  console.log 'express app started'
)

#should add after the router middleware
app.use(express.errorHandler())

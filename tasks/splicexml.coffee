module.exports = (grunt)->
  fs = require 'fs'
  chalk = require 'chalk'
  startMarker = "[s"
  endMarker = "]"
  dest = null
  out = null

  readLines = (input, id, callback)->
    #console.log "readLines", id
    str = ""
    #out = ""
    input.on 'data', (data)->
      str += data
    input.on 'end', ()->
        join str, id, callback

  join = (str, id, callback)->
    str = str.substring str.indexOf("<body>")+6, str.indexOf("</body>")
    str = str.replace(/^\s+|\s+$/g, '')
    #console.log str
    out += "  <#{id}>\n    <![CDATA[#{str}]]>\n  </#{id}>\n"

    callback()



  grunt.registerMultiTask 'splicexml', 'Join html bodies together in 1 xml', ->
    done = @async()
    paths = []
    tally = 0
    out = "<texts>\n"
    #console.log @options()
    @files.forEach (file)->
      #console.log file
      dest = file.orig.dest
      file.src.forEach (path)->
        if grunt.file.exists path
          tally++
          paths.push path
    callback = (current_file, current_path)->
      tally--
      if tally is 0
        out += "</texts>\n"
        fs.writeFileSync dest, out
        grunt.log.writeln "File #{chalk.cyan(dest)} created."
        done()
    if paths.length>0
      for path in paths
        input = fs.createReadStream path
        id = path.substring path.lastIndexOf("/")+1, path.lastIndexOf(".")
        readLines input, id, callback
    else
      grunt.log.writeln "No html files to split."
      done()